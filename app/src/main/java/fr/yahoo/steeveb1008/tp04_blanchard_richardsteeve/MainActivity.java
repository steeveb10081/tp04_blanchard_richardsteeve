package fr.yahoo.steeveb1008.tp04_blanchard_richardsteeve;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {


    Button butt_ouvrir ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("CDV MainActivity", "La méthode onCreate est appelée");

        butt_ouvrir=findViewById(R.id.butt_ouvrir);

        butt_ouvrir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SecondActivity.class));
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("CDV MainActivity", "La méthode onStart est appelée");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("CDV MainActivity", "La méthode onResume est appelée");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("CDV MainActivity", "La méthode onPause est appelée");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("CDV MainActivity", "La méthode onStop est appelée");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("CDV MainActivity", "La méthode onRestart est appelée");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("CDV MainActivity", "La méthode onDestroy est appelée");
    }



}